package mysql

import (
	"context"
	"database/sql"
	"strings"
	"sync"
	"time"

	"bitbucket.org/losaped/scrapper"
)

const queryTemplate = `insert into stats (client_ip, request_time, request) values `
const maxBufLen = 1000

type buffer struct {
	mu            sync.Mutex
	con           *sql.DB
	valStatements []string
	vals          []interface{}
	errChan       chan error
}

func (buf *buffer) append(stat scrapper.Stat) {

	buf.mu.Lock()

	buf.valStatements = append(buf.valStatements, "(?,?,?)")
	buf.vals = append(buf.vals, stat.ClientIP, stat.RequestTime, stat.Service)

	buf.mu.Unlock()

	if len(buf.vals) >= maxBufLen {
		buf.exec()
	}
}

func (buf *buffer) start(ctx context.Context, con *sql.DB, duration time.Duration) <-chan error {
	buf.con = con
	buf.errChan = make(chan error, 1)

	go func() {
		defer close(buf.errChan)
		ticker := time.NewTicker(duration)

		for {
			select {
			case <-ticker.C:
				buf.exec()
			case <-ctx.Done():
				ticker.Stop()
				buf.exec()
				return
			}
		}
	}()

	return buf.errChan
}

func (buf *buffer) exec() {
	buf.mu.Lock()
	if len(buf.vals) > 0 {
		statement, _ := buf.con.Prepare(buf.combineQuery())
		if _, err := statement.Exec(buf.vals...); err != nil {
			buf.errChan <- err
		}
		statement.Close()
	}

	buf.createBuffers()

	buf.mu.Unlock()
}

func (buf *buffer) combineQuery() string {
	return queryTemplate + strings.Join(buf.valStatements, ",")
}

func (buf *buffer) createBuffers() {
	buf.vals = make([]interface{}, 0, maxBufLen*3)
	buf.valStatements = make([]string, 0, maxBufLen)
}
