package mysql

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
	log "github.com/sirupsen/logrus"

	"bitbucket.org/losaped/scrapper"
	"bitbucket.org/losaped/scrapper/config"
)

var (
	errNotInitialized = errors.New("repository not initialized")
)

func connString(cnf config.DBConfig) string {
	return fmt.Sprintf("%s:%s@tcp(%s)/%s?parseTime=true", cnf.DBUser, cnf.DBPassword, cnf.DBHost, cnf.DBName)
}

// CreateStatRepo creates instance of StatisticsRepository
// ctx context.WithCancel allow stopping routine for batch insert into stats table
// config contains options to connect
// initdb run db initialization
// logger is a logger instance
func CreateStatRepo(ctx context.Context, config config.DBConfig, initdb bool, logger *log.Entry) (*StatisticsRepository, error) {
	conString := connString(config)

	db, err := sql.Open("mysql", conString)
	if err != nil {
		logger.WithFields(log.Fields{"error": err, "con_string": conString}).Error("on connect to db")
		return nil, err
	}

	repo := &StatisticsRepository{
		con:    db,
		logger: logger,
	}

	if initdb {
		if err := repo.Init(); err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("on init db")
			return nil, err
		}

		logger.Info("database initialized")
	}

	buf := &buffer{}
	bufErrs := buf.start(ctx, db, time.Second)

	go func() {
		for err := range bufErrs {
			logger.WithFields(log.Fields{"buffer_error": err}).Error("from buffer")
		}
	}()

	repo.buf = buf
	return repo, nil
}

type StatisticsRepository struct {
	con    *sql.DB
	logger *log.Entry
	buf    *buffer
}

func (sr *StatisticsRepository) Init() error {
	if _, err := sr.con.Exec(`CREATE TABLE stats (
		id integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
		client_ip varchar(255) NOT NULL,
		request_time datetime NOT NULL,
		service varchar(255) NOT NULL,
		key(service)
  )`); err != nil {
		sr.logger.WithFields(log.Fields{"error": err}).Error("on creating statistic table")
		return err
	}

	return nil
}

func (sr *StatisticsRepository) GetStats(service string) ([]scrapper.Stat, error) {
	rows, err := sr.con.Query("select client_ip, request_time, service from stats where service = ?", service)
	if err != nil {
		sr.logger.WithFields(log.Fields{"error": err, "service_name": service}).Error("on getting service stats from db")
		return nil, err
	}

	defer rows.Close()

	stats := []scrapper.Stat{}
	for rows.Next() {
		var stat scrapper.Stat
		if err := rows.Scan(&stat.ClientIP, &stat.RequestTime, &stat.Service); err != nil {
			sr.logger.WithFields(log.Fields{"error": err, "service_name": service}).Error("on scan rows")
			return nil, err
		}

		stats = append(stats, stat)
	}

	return stats, nil
}

func (sr *StatisticsRepository) SaveStats(stat scrapper.Stat) error {
	if sr.buf == nil {
		return errNotInitialized
	}

	sr.buf.append(stat)
	return nil
}
