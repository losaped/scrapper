package redis

import (
	"sync"
	"time"

	"bitbucket.org/losaped/scrapper"
	"github.com/gomodule/redigo/redis"
	log "github.com/sirupsen/logrus"
)

const (
	hmset     = "hmset"
	hmget     = "hmget"
	scrapping = "scrapping"
)

func newPool(addr string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		IdleTimeout: 240 * time.Second,
		Dial:        func() (redis.Conn, error) { return redis.Dial("tcp", addr) },
	}
}

func CreateScrappingRepo(redisAddr string, logger *log.Entry) scrapper.ScrappingRepository {
	return &ScrappingRepository{
		pool: newPool(redisAddr),
	}
}

type ScrappingRepository struct {
	pool *redis.Pool
	mu   sync.RWMutex
	max  *scrapper.Result
	min  *scrapper.Result
}

func (sr *ScrappingRepository) checkRanges(result scrapper.Result) {
	sr.mu.Lock()
	defer sr.mu.Unlock()

	if sr.min == nil || result.ResponseTime < sr.min.ResponseTime {
		sr.min = &result
	}

	if sr.max == nil || result.ResponseTime > sr.max.ResponseTime {
		sr.max = &result
	}
}

func (sr *ScrappingRepository) Save(result scrapper.Result) error {
	con := sr.pool.Get()
	defer con.Close()
	_, err := con.Do(hmset, scrapping, result.Service, result.ResponseTime)
	if err != nil {
		log.WithFields(log.Fields{"service": result.Service}).Error("on saving scrapping result")
		return err
	}

	sr.checkRanges(result)
	return err
}

func (sr *ScrappingRepository) GetService(name string) (*scrapper.Result, error) {
	con := sr.pool.Get()
	defer con.Close()
	time, err := redis.Int(con.Do(hmget, scrapping, name))
	if err != nil {
		log.WithFields(log.Fields{"service": name}).Error("on getting result")
		return nil, err
	}

	return &scrapper.Result{Service: name, ResponseTime: int64(time)}, err
}

func (sr *ScrappingRepository) GetSlow() (*scrapper.Result, error) {
	sr.mu.RLock()
	defer sr.mu.RUnlock()
	if sr.max == nil {
		return nil, scrapper.ErrValueNotFound
	}

	return sr.max, nil
}

func (sr *ScrappingRepository) GetFast() (*scrapper.Result, error) {
	sr.mu.RLock()
	defer sr.mu.RUnlock()
	if sr.min == nil {
		return nil, scrapper.ErrValueNotFound
	}

	return sr.min, nil
}
