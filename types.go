package scrapper

import (
	"errors"
	"time"
)

var ErrValueNotFound = errors.New("value not found")

// Result struct represents result of service availability
type Result struct {
	Service      string `json:"service"`
	ResponseTime int64  `json:"response_time"` // millisecond
}

// Stat struct represents info about usage stat
type Stat struct {
	ClientIP    string    `json:"client_ip"`
	RequestTime time.Time `json:"request_time"`
	Service     string    `json:"service"`
}

// ScrappingRepository allow save and get services availability
type ScrappingRepository interface {
	Save(result Result) error
	GetService(name string) (*Result, error)
	GetSlow() (*Result, error)
	GetFast() (*Result, error)
}

// StatisticsRepository allow save and get stats of user usages
type StatisticsRepository interface {
	GetStats(service string) ([]Stat, error)
	SaveStats(Stat) error
}

// TaskRepository allow read and return chan with tasks
type TaskRepository interface {
	GetTasks() (<-chan string, error)
}
