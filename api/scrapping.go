package api

import (
	"errors"
	"net/http"
	"time"

	"bitbucket.org/losaped/scrapper"
	"github.com/gin-gonic/gin"
)

var ErrServiceNameRequired = errors.New("service name required")

func (api *Api) slowService(c *gin.Context) {
	res, err := api.Results.GetSlow()
	if err != nil {
		if err == scrapper.ErrValueNotFound {
			c.JSON(http.StatusNotFound, gin.H{"error": scrapper.ErrValueNotFound.Error()})
			return
		}
	}

	c.JSON(http.StatusOK, *res)
}

func (api *Api) fastService(c *gin.Context) {
	res, err := api.Results.GetFast()
	if err != nil {
		if err == scrapper.ErrValueNotFound {
			c.JSON(http.StatusNotFound, gin.H{"error": scrapper.ErrValueNotFound.Error()})
			return
		}
	}

	c.JSON(http.StatusOK, *res)
}

func (api *Api) getServiceInfo(c *gin.Context) {
	name := c.Param("name")
	if len(name) == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": ErrServiceNameRequired.Error()})
		return
	}

	api.Stats.SaveStats(scrapper.Stat{
		ClientIP:    c.Request.RemoteAddr,
		RequestTime: time.Now(),
		Service:     name,
	})

	res, err := api.Results.GetService(name)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"errr": err})
		return
	}

	c.JSON(http.StatusOK, res)
}
