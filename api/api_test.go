package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/losaped/scrapper"

	scrmock "bitbucket.org/losaped/scrapper/mock"
)

func TestSlow(t *testing.T) {
	api := Api{
		Results: &scrmock.ScrappingRepository{
			GetSlowFn: func() (*scrapper.Result, error) {
				return &scrapper.Result{
					Service:      "slow service",
					ResponseTime: 900,
				}, nil
			},
		},
		Stats: &scrmock.StatisticsRepository{},
	}

	req, _ := http.NewRequest("GET", "/slow-service", nil)
	w := httptest.NewRecorder()

	router := api.Router()
	router.ServeHTTP(w, req)
	if w.Code != http.StatusOK {
		t.Errorf("Status code should be %v, was %d", http.StatusOK, w.Code)
	}

	was := w.Body.String()
	should := `{"service":"slow service","response_time":900}`
	if was != should {
		t.Errorf("response should be %s, was %s", should, was)
	}
}

func TestFast(t *testing.T) {
	api := Api{
		Results: &scrmock.ScrappingRepository{
			GetFastFn: func() (*scrapper.Result, error) {
				return &scrapper.Result{
					Service:      "fast service",
					ResponseTime: 0,
				}, nil
			},
		},
		Stats: &scrmock.StatisticsRepository{},
	}

	req, _ := http.NewRequest("GET", "/fast-service", nil)
	w := httptest.NewRecorder()

	router := api.Router()
	router.ServeHTTP(w, req)
	if w.Code != http.StatusOK {
		t.Errorf("Status code should be %v, was %d", http.StatusOK, w.Code)
	}

	was := w.Body.String()
	should := `{"service":"fast service","response_time":0}`
	if was != should {
		t.Errorf("response should be %s, was %s", should, was)
	}
}

func TestSlowWithError(t *testing.T) {
	api := Api{
		Results: &scrmock.ScrappingRepository{
			GetSlowFn: func() (*scrapper.Result, error) {
				return nil, scrapper.ErrValueNotFound
			},
		},
		Stats: &scrmock.StatisticsRepository{},
	}

	req, _ := http.NewRequest("GET", "/slow-service", nil)
	w := httptest.NewRecorder()

	router := api.Router()
	router.ServeHTTP(w, req)
	if w.Code != http.StatusNotFound {
		t.Errorf("Status code should be %v, was %d", http.StatusNotFound, w.Code)
	}

	was := w.Body.String()
	should := `{"error":"value not found"}`
	if was != should {
		t.Errorf("response should be %s, was %s", should, was)
	}
}

func TestServiceWithStatistics(t *testing.T) {
	store := make(map[string]*scrapper.Stat)

	api := Api{
		Results: &scrmock.ScrappingRepository{
			GetServiceFn: func(service string) (*scrapper.Result, error) {
				return &scrapper.Result{
					Service:      service,
					ResponseTime: 300,
				}, nil
			},
		},
		Stats: &scrmock.StatisticsRepository{
			SaveStatsFn: func(stat scrapper.Stat) error {
				store[stat.Service] = &stat
				return nil
			},

			GetStatsFn: func(service string) ([]scrapper.Stat, error) {
				fmt.Println(*store[service])
				return []scrapper.Stat{*store[service]}, nil
			},
		},
	}

	req, _ := http.NewRequest("GET", "/service/google", nil)
	w := httptest.NewRecorder()

	router := api.Router()
	router.ServeHTTP(w, req)
	if w.Code != http.StatusOK {
		t.Errorf("Status code should be %d, was %d", http.StatusOK, w.Code)
		return
	}

	was := w.Body.String()
	should := `{"service":"google","response_time":300}`
	if was != should {
		t.Errorf("response should be %s, was %s", should, was)
		return
	}

	//==========================================
	req, _ = http.NewRequest("GET", "/admin/stats/services/google", nil)
	req.SetBasicAuth("admin", "adminpass")
	w = httptest.NewRecorder()

	router.ServeHTTP(w, req)
	if w.Code != http.StatusOK {
		t.Errorf("Status code should be %d, was %d", http.StatusOK, w.Code)
		return
	}

	was = w.Body.String()
	shouldBts, _ := json.Marshal([]scrapper.Stat{*store["google"]})
	should = string(shouldBts)
	if was != should {
		t.Errorf("response should be %s, was %s", should, was)
		return
	}
}
