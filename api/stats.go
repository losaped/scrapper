package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func (api *Api) getServiceStats(c *gin.Context) {
	service := c.Param("name")
	if len(service) == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": ErrServiceNameRequired.Error()})
		return
	}

	stats, err := api.Stats.GetStats(service)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err})
		return
	}

	c.JSON(http.StatusOK, stats)
}
