package api

import (
	"bitbucket.org/losaped/scrapper"
	"github.com/gin-gonic/gin"
)

// Api contains repo interface implementations
type Api struct {
	Results scrapper.ScrappingRepository
	Stats   scrapper.StatisticsRepository
}

// Router creates router
func (api Api) Router() *gin.Engine {
	router := gin.Default()
	router.GET("/service/:name", api.getServiceInfo)
	router.GET("/slow-service", api.slowService)
	router.GET("/fast-service", api.fastService)

	admin := router.Group("/admin", gin.BasicAuth(gin.Accounts{
		"admin": "adminpass",
	}))

	admin.GET("/stats/services/:name", api.getServiceStats)
	return router
}
