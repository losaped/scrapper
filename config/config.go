package config

import (
	"os"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
)

type Config struct {
	RedisAddress       string        // addres for redis connection
	TaskFile           string        // path to file with services list
	APIAddress         string        // api listener address
	TaskUpdateSeconds  time.Duration // duration between scrapping in seconds
	TaskProcessTimeout time.Duration // timeout service scrapping in seconds
	DBConfig           DBConfig
	TaskChanDimension  int
	NumberOfWorkers    int // number of workers than process tasks
}

type DBConfig struct {
	DBUser     string
	DBPassword string
	DBHost     string
	DBName     string
}

func FillConfig() Config {
	cfg := Config{
		RedisAddress: os.Getenv("REDIS_ADDR"),
		TaskFile:     os.Getenv("TASK_FILE"),
		APIAddress:   os.Getenv("API_ADDRESS"),
	}

	tus, err := strconv.Atoi(os.Getenv("TASK_UPDATE_SECOND"))
	if err != nil {
		log.WithFields(log.Fields{"errror": err}).Error("on convert TASK_UPDATE_SECOND")
		os.Exit(1)
	}

	cfg.TaskUpdateSeconds = time.Second * time.Duration(tus)

	tpt, err := strconv.Atoi(os.Getenv("TASK_TIMEOUT"))
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error("on convert TASK_TIMEOUT")
		os.Exit(1)
	}

	cfg.TaskProcessTimeout = time.Second * time.Duration(tpt)

	tcd, err := strconv.Atoi(os.Getenv("TASK_CHAN_DIMENSION"))
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error("on convert TASK_CHAN_DIMENSION")
		os.Exit(1)
	}

	cfg.TaskChanDimension = tcd

	nw, err := strconv.Atoi(os.Getenv("NUM_WORKERS"))
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error("on convert NUM_WORKERS")
		os.Exit(1)
	}

	cfg.TaskChanDimension = nw

	cfg.DBConfig = DBConfig{
		DBUser:     os.Getenv("DB_USER"),
		DBPassword: os.Getenv("DB_PASS"),
		DBHost:     os.Getenv("DB_HOST"),
		DBName:     os.Getenv("DB_NAME"),
	}

	return cfg
}
