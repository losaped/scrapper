package scrapper

import (
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
)

const schema = "http://"

type TaskProcessor struct {
	ScrapRepo ScrappingRepository
	*log.Entry
	NumberOfWorkers int
	Timeout         time.Duration //таймаут для проверки каждого сервиса в секундах
}

func (tp TaskProcessor) processTask(task string) error {
	c := http.Client{
		Timeout: tp.Timeout,
	}

	result := Result{Service: task}
	start := time.Now()

	_, err := c.Get(schema + task)
	if err != nil {
		tp.WithFields(log.Fields{"error": err}).Error("on send get to service")
		result.ResponseTime = 0
	} else {
		result.ResponseTime = int64(time.Since(start))
	}

	if err := tp.ScrapRepo.Save(result); err != nil {
		tp.WithFields(log.Fields{"from": "task_processor", "error": err, "service": task}).Error("on saving task result")
		return err
	}

	return nil
}

// Process run workers for process tasks
func (tp TaskProcessor) Process(tasks <-chan string) {
	for i := 0; i < tp.NumberOfWorkers; i++ {
		go func(workerNum int) {
			for task := range tasks {
				tp.processTask(task)
			}
		}(i)
	}
}
