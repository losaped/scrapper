FROM golang:latest as builder

WORKDIR /go/src/bitbucket.org/losaped/scrapper
COPY . .

WORKDIR /go/src/bitbucket.org/losaped/scrapper/cmd
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./scrapper ./cmd


FROM alpine:latest

RUN apk --no-cache add ca-certificates

RUN mkdir /app
WORKDIR /app
COPY --from=builder /go/src/bitbucket.org/losaped/scrapper .

#cmd ["./cmd/cmd" "-initdb"]