package file

import (
	"bufio"

	"os"

	"bitbucket.org/losaped/scrapper"
	log "github.com/sirupsen/logrus"
)

// FileTasksRepository implements scrapper.TaskRepository throught file store
type FileTasksRepository struct {
	*log.Entry
	chanDimension int
	fileName      string
}

// CreateTasksRepo creates FileTaskRepository
func CreateTasksRepo(filepath string, chanDim int, logger *log.Entry) scrapper.TaskRepository {
	return FileTasksRepository{
		chanDimension: chanDim,
		fileName:      filepath,
		Entry:         logger,
	}
}

// GetTasks read file and send tasks to channel
func (tr FileTasksRepository) GetTasks() (<-chan string, error) {
	file, err := os.Open(tr.fileName)
	if err != nil {
		tr.WithFields(log.Fields{"error": err, "file_path": tr.fileName}).Error("on reading tasks file")
		return nil, err
	}

	ch := make(chan string, tr.chanDimension)
	go func() {
		defer func() {
			file.Close()
			close(ch)
		}()

		taskReader := bufio.NewScanner(file)

		for taskReader.Scan() {
			ch <- taskReader.Text()
		}
	}()

	return ch, nil
}
