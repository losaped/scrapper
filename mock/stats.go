package mock

import (
	"bitbucket.org/losaped/scrapper"
)

type StatisticsRepository struct {
	GetStatsFn  func(service string) ([]scrapper.Stat, error)
	SaveStatsFn func(stat scrapper.Stat) error

	GetStatCalled   bool
	SaveStatsCalled bool
}

func (sr *StatisticsRepository) GetStats(service string) ([]scrapper.Stat, error) {
	sr.GetStatCalled = true
	return sr.GetStatsFn(service)
}

func (sr *StatisticsRepository) SaveStats(stat scrapper.Stat) error {
	sr.SaveStatsCalled = true
	return sr.SaveStatsFn(stat)
}
