package mock

import (
	"bitbucket.org/losaped/scrapper"
)

type ScrappingRepository struct {
	SaveFn       func(result scrapper.Result) error
	GetServiceFn func(name string) (*scrapper.Result, error)
	GetSlowFn    func() (*scrapper.Result, error)
	GetFastFn    func() (*scrapper.Result, error)

	SaveCalled       bool
	GetServiceCalled bool
	GetSlowCalled    bool
	GetFastCalled    bool
}

func (sr *ScrappingRepository) Save(result scrapper.Result) error {
	sr.SaveCalled = true
	return sr.SaveFn(result)
}

func (sr *ScrappingRepository) GetService(name string) (*scrapper.Result, error) {
	sr.GetServiceCalled = true
	return sr.GetServiceFn(name)
}

func (sr *ScrappingRepository) GetSlow() (*scrapper.Result, error) {
	sr.GetSlowCalled = true
	return sr.GetSlowFn()
}

func (sr *ScrappingRepository) GetFast() (*scrapper.Result, error) {
	sr.GetFastCalled = true
	return sr.GetFastFn()
}
