build:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./scrapper ./cmd

run:
	REDIS_ADDR=localhost:6379 \
	TASK_FILE=./tasks/services.txt \
	API_ADDRESS=localhost:8080 \
	TASK_UPDATE_SECOND=30 \
	TASK_TIMEOUT=5 \
	TASK_CHAN_DIMENSION=5 \
	NUM_WORKERS=10 \
	DB_USER=root \
	DB_PASS=example \
	DB_HOST=localhost:3306 \
	DB_NAME=scrapperdb ./scrapper