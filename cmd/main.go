package main

import (
	"context"
	"flag"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/losaped/scrapper"
	"bitbucket.org/losaped/scrapper/api"
	"bitbucket.org/losaped/scrapper/config"
	"bitbucket.org/losaped/scrapper/file"
	"bitbucket.org/losaped/scrapper/mysql"
	"bitbucket.org/losaped/scrapper/redis"
	log "github.com/sirupsen/logrus"
)

func main() {
	var initStatsDB bool
	flag.BoolVar(&initStatsDB, "initdb", false, "-initdb")
	flag.Parse()

	config := config.FillConfig()

	sigs := make(chan os.Signal, 1)
	doneCtx, doneCtxFunc := context.WithCancel(context.Background())

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	// init repositories
	taskRepo := file.CreateTasksRepo(config.TaskFile, config.TaskChanDimension, log.WithFields(log.Fields{"repo": "file_tasks"}))
	scrapRepo := redis.CreateScrappingRepo(config.RedisAddress, log.WithFields(log.Fields{"repo": "scrapping_result"}))
	statRepo, err := mysql.CreateStatRepo(doneCtx, config.DBConfig, initStatsDB, log.WithFields(log.Fields{"repo": "statistics"}))
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}

	tasksProcessor := scrapper.TaskProcessor{
		ScrapRepo: scrapRepo,
		Timeout:   config.TaskProcessTimeout,
		Entry:     log.WithFields(log.Fields{"service": "task_processor"}),
	}

	api := api.Api{
		Results: scrapRepo,
		Stats:   statRepo,
	}

	srv := &http.Server{
		Addr:    config.APIAddress,
		Handler: api.Router(),
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// run scrapping by ticker
	// stopp process by os signal
	ticker := time.NewTicker(config.TaskUpdateSeconds)
	go func() {

		for range ticker.C {

			tasks, err := taskRepo.GetTasks()
			if err != nil {
				log.WithFields(log.Fields{"error": err}).Error("on getting tasks")
				os.Exit(1)
			}

			tasksProcessor.Process(tasks)
		}
	}()

	<-sigs
	ticker.Stop()
	doneCtxFunc()

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("server shutdown: %s", err)
	}

	log.Info("server stopped")
}
